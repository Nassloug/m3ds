#version 130

uniform float coeff;

in vec3 position;
in vec4 color;
in vec2 texCoord;

out vec4 inColor;
out vec2 fTexCoord;
        
void main() {
    vec3 newPosition = position*coeff;
    inColor=color;
    fTexCoord = texCoord;
    gl_Position=vec4(newPosition,1.0);
}
