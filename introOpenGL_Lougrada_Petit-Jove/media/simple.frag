#version 130

uniform sampler2D texture;

in vec4 inColor;
out vec4 fragColor;
in vec2 fTexCoord;
        
void main() {
    fragColor=inColor;
    fragColor=texture2D(texture,fTexCoord);
}
